#Routage Hierarchique


Most topology that have been presented thus far are mostly virtual and very theoretical

For example, they all assume that each router is the same, which is just impossible in real life, especially when talking about the Internet.

Issue of size:
-It would be really hard to stock all the nodes of the internet in every route	
-There's just way too many

Issue of maintenance:
-The interconnected nature of the internet doesn't allow for a global routing system, as each admin would like to be able to route it's network the wya he wants it.

One solution is to divide the set of nodes in sub-region, called autonomous system(AS)
Where each node in an AS has the same routing protocol.

For each AS, we'd find two type of nodes:
	-intra-AS
	-gateway-nodes (nodes that are connected to other AS)

Each node in an AS would have two routing tables, one for the inter-AS destination and one for the intra-AS.

In the case where a node must transfer a datagram outside it's own AS, it has to choose which one of it's neighbors AS it could use, so it's fowarding table has to know what are the destinations that are reachable by each neighbors...


#RIP (routing information protocol)

Def: RIP is a protocol for INTRA-AS datagram transfer, under the umbrella of interior 
gateway protocol categories.

It relies on a distance vector algorithm, and it's metric of distance(criteria) is 
the number of HOP and the cost of each link.

It exchange information every 30 seconds in special messages

It has a special failsafe in case a link is deactivated without notice, if after 180 
secondes, a node doesn't hear about one of it's neighbors, it considers it as dead et
considers any path going trough it as invalid...

It then send advertisment to it's neighbors to make sure no one goes trought there, this is a process that is done recursively.

It implement the poison reverse technique by associating a distance == infinity with 16 jumps.

In RIP, each forwading table are managed by a processus called route-d, wich sends 
announces via UDP.

#OSPF (OPEN SHORTED PATH FIRST)

DEF: it's a protocol for INTRA-AS datagram transfer, it is open and accesible to the 
public.

It is based on a LINKED STATE algorithm, and uses the Dijsktra algorithms.

During the flooding phase, it uses IP datagram directly and announces a change as an 
event, or it does it every 30 minutes.

It offers specifics that aren't offered by RIP:
-security and authetification of every messages
-multiple paths with the same cost are permitted(this is done use the TOS field in the datagram)
-it supports hierarchical routing.

In OSPF, the hierarchical routing is done in two level: local and dorsale.

local nodes are subnets with one entry point, while dorsale nodes are connectiong 
multiple local nodes together.
At the top of the hierarchy, one finds the main node which connects the dorsales 
togethers, and to the other AS.


#BGP (border gateway protocol)

def: this protocol is specific to inter-AS, it provides 2 services:
-eBGP: exchange data between neighbors (inter-AS)
-iBGP: propagate data between node of specific AS. (intra-AS)

It works a bit like in an INTRA-AS protocol, a gateway node will announces that he can reach another gateway node, and thus promise that every node that wants to reach that
other subnet will be able to do it via him.

During the transmission of the BGP message, there are 2 attribute that are important other thant the prefixe of the subnet that are reachable.
1)AS-PATH: the name of the other AS the message has been passed already
2)NEXT-HOP: the interface adress of the gateway responsible for the transfert of 
packets.

It is possible for a node to reject an announce.

There a 4 type of BGP messages:
-open -> to self authentify to another gateway
-update -> to announce a new path (or remove)
-KeepAlive -> to ACK a received open message
-Notification -> to report error and closing connections.


#Differences

There are multiple reason for different INTER and INTRA routing:

-An admin may want to controle how/what traffic goes trough his network.
-Only one admin per INTRA AS means no big decisions have to be made.

-The biggest reason is the scalability, a large list of small AS is more manageable because
it allows for smaller forwarding tables.

- a routing INTRA allows for faster performance
- a routing INTER-AS has a bigger focus on politics thant performance (who has the right to go 
trough an AS)








