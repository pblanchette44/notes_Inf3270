#Exercice 1

~~~
Completé le nombre tableau des sous réseaux
1 120 adresses
2 250 adresses
3 100 adresses
4 248 adresses
5 60 adresses
6 50 adresses

Adresse du réseau 162.68.120.0/22
~~~

Notes:

/22 means

10 bits for 6 subnets

ex: if 120 adresses 255.255.252

120 -> 128 (couple be couple with 3 ?)
250 -> 255
100 -> 128 (could be coupled ?)
248 -> 255
60  -> 64 (could be coupled)
50  -> 64 (could be coupled)

0000 0000


162.68.120.0/22
1010 0010 . 0100 0100 . 0111 10|00 . 0000 0000
|
1111 1111 . 1111 1111 . 1111 11|00 . 0000 0000
255.255.252.0			
bit associated with net
				42   1

on peut dire
162.68.120.0/22 = reseau 1
10 bit for adressing == 1791 adresses... way too much

162.68.121.0/24 = reseau 2	(250 adress)
8 bits for adresses = 255 adresses

162.68.122.0/24 = reseau 3	(100 adress)
8 bit = 255

162.68.123.0/24 = reseau 4	(248 adress)
8 bit = 255

162.68.124.0/24 = reseau 5	(60 adress)
255 adress

162.68.125.0/24 = reseau 6	(50 adress)
255 adress


network 3-5-6 could be coupled under a common prefix of /24
example
say we give
net3(100 adress)
0-100
162.68.120.0/25
255.255.255.64
0110 0100 
net5(60)
162.68.120.64/26
101-161
0110 0100 (63 adress)
net5 (50)
162-212
11|00 0000
162.68.120.192/26

net1 (100 adress -> 128)
162.68.121.128/25
3rd byte = 0111 1001
last byte 1000 0000

net2 (250 adress -> 255)
162.68.122.0/24

net4 (248 adress 255)
162.68.123.0/24

net1:	162.68.121.128/25
net2:	162.68.122.0/24
net3:	162.68.120.0/25
net4:	162.68.123.0/24
net5:	162.68.120.64/26
net6:	162.68.120.192/26

"I think this is the answer ?"




entry point are the largest adress minus the number ?
subnet 1 has an adress range of
162.68.121.128 <-> 162.69.121.255 = width of 127

162.69.121.255 is broadcast
R1 interface = 162.69.121.253
R2 interface = 162.69.121.254

there's 1 unused IP
Subnet Adress	:162.69.121.128
Host range	:162.69.121.129 <-> 162.69.121.249  
TO other subnet :162.69.121.253 && 162.69.121.254
Broadcast	:162.69.121.255
Unused		:162.69.121.250 <-> 162.69.121.252 

