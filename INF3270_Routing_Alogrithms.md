#Link State Algorithm

goal: make sure every node knows the quickest way to everynoe in the network topology.

execution: 2 phases

1st phase:
Message flooding.

2nt phase:
Dijsktra algorithm.

~~~
1st phase

the goal of the first phase is to "map" the topology, by having each node send a
message to it's neighbors.

The content of the message is the distance between each node (like in a graph).

To prevent certain problems (like infinite looping,among others):
Link state messages possess:
-a unique ID
-a TTL
-a sequence number

These three identifier help each node in the ordering and prioritising of the message, which may arrive in an unorderered fashion.
~~~

The second phase is a dijsktra algorithm, so it's mostly about navigating a graph.

L'objectif d'un protocol de routage est de determiner le bon chemin à prendre au travers d'un réseau de routeurs.

We associate a "good" path by it's distance/traversal cost/congestion level

A Link state algorithm is a "global" or "centralized" routing algorithm, since every
node willl know the whole topology of the network.

Whereas a decentralized algorithm will provide minimal information about the topology and mostly limit's a node knowledge to it's own neighbors only.


The dijsktra algorithm will be used to build what is called a transfer table, which will be stored inside the router.


#static and dynamic topology
static == the paths change rarely.
dynamiq == the path change periodicaly, often in response to the changing cost of the link state (like the internet).



#Distance vector algorithm

decentralized, asynchrone and iterative

meaning that it can be trigger by any node individualy, when there is a changing cost
in link or a change in the neigbor, it is a distributed algorithm since every node 
informs it's neighbors.

it mostly loops trought the followign 3 steps

wait(for a change(local/neigbors)) --timeout--> reevaluate cost --changed--> broadcast 
							|
							V
						go back to wait


To avoid infinite loops, we use the poisoned reversed technique:
a node A goes trough B to reach C
A tells B that it's distance to C is infinite, so B won't use C to go tp A.



#Comparison

there are three criteria for comparisons:

Messages exchanges
	
	Link-state exchange a number of message equals to n*E 
	n == number of nodes
	E == number of Links
	
	Distance vector:
	uniquely exchange between neighbors, meaning it's execution time varies but is 		definitely faster thant Link-state

Convergence

	Link-State: O(n2) et O(nE) message

	Distance vectore: potentiellement infini, mais aussi potentiellement vraiment
	vite

reliability
	
	Link-state:
		each node only compute it's own table
		each node can possibly send flase info
	Distance vector:
		each node can possibly send false info, and it uses the table of 
		it's neighbors so it opens up the possibility of a lot of error 
		propagation

