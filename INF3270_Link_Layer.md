#Link Layer

topic:
	the dtata link layer is in charge of bridging the logical and digital realm
	with the physical medium, it is often divided in two sublayer the logical and physical

	One great aspect of the link layer is that it follows closely an encapsulation principle
	and can take care of the Frame packing, regardless of the implementation of the upper layer 		protocols.

	This is great since it is supposed to know how to translate to any form of media 
	which would be a pain to maintain if you had to make a protocol for each upper layer 
	protocol to media protoocol.

	The name of the link layer packet is the ETHERNET FRAME !!! it contains as little info as 
	possible, since the routing is mostly already chosen, it takes a physical adress as 
	destination and source, as well as a checksum that is used AT EVERY NODE for CRC error 
	checking.

	The link layer is also taking care of collision ovetr the media via a standard name CSMA
	for Carrier Sense Multiple Access.
	there are two implementation of CSMA
	CSMA/CA  collision avoidance
	CSMA/CD  collision detection
	
#Error detection and correction Techniques	


The most basic form of error detection is the parity check:
	- it basicaly consists of counting the number of bits == 1 in the message and if it's
	  even set a specific parity bits (which is basicaly a flag) to 1.
	- All the receiver has to do is to chekc for the value of the parity bit, and if it's equal
	  to 1, it counts the number of 1 bits in the message, if the number is not even, then at least 	  one error has occured (which is all you need to know to ask for a resend...)
	- Note that a parity check can be even or odd...
	- Also note that it doesn't protect against an even number of error, but more than one error
		in a frame is a very marginal possibility.

This parity check is not sufficient for real life, and a device called a two-dimensional parity check
is perfect to remedy to just that, the only drawback is that it takes more than one parity bit.

for a message of the form  1001 1010 1100

step 1:
	make a matrix with row as the byte and col as the bit
		
		1001
		1010
		1100
step 2:
	make the parity of the rows and columns

		1001 0 
		1010 0	
		1100 0		
		
		1111 0 	

step 3:
	pack the resulting parity in the checksum field length is width+height+1
		1111 0000


now when receiving the message, the receiver does a reverse matrix

		1011 0
		1010 0
		1100 0
		
		1111 0
*note that an error has been put at the 0,2 position

	the receiver can do a parity check for each row and colums
		  V
		1011 1 x
		1010 0
		1100 0
		
		1101 0
		  x	
	an erro has been detected and cna even be corrected because we know exactly which bit is wrong.


*/ This ability to correct two dimensional error, is calle dFEC or Forward Error checking.
This is often used with Audio streaming... It's a quick way to detect error and correct it without 
needing to use the NAK delay of the upper layers...

#CRC or cyclic redundancy check

okay so basicaly CRC is a way to do error detection:

for a message D, a sequence G of length r+1 
(where r is equal to the length of a pattern R such as D+R%G == 0)

The %2 is without carries or borrows and is thus a XOR operation

G     D      R
----  ------ ---
1001  101110 000
	





