#Multiple Acces protocols

Basicaly, multiple access protocols are sets of rules to determines who gets to use a link at a giventime in a network.

Of course, if you have multiple host in a network, they need to agree on a way to use the shared 
media (the link) without causing colisions.

There are 3 categories of Access Protocols in the broadcast link category:

Random
Controlled
Channeled

There a five caracteristics a multiple access protocol should have:
	- only one node transmits == troughput of R
	- thus M nodes transmits == troughput of R/M for each
	- R/M is an average of course...
	- the protocol is decentralized and theres no "master node"
	- the protocol is simple to implement.

#Channel partionning protocols:

- Time division multiplexing

Basicaly, divide the time in time frame which are again divided in time-slots assigned to each host.
this method is great for a network where multiple host a often send info at the same time, because itlocks each other in a specific time slot and stops collision altogether.

But it's not viable for a situation where only one host is broadcasting, since it's only using a fraction of the bandwitdh...

	
- frequency division multiplexing

The same principle of TDM, except this time it's the frequency thats divided, so everyone can broadcast but only usinf a fraction of the bandwidth, again viable when multiple host want to broadcast 
small messages at the same time.

but unviable when one host wants to use all the bandwidth.
	

-CDMA or code division multiple access

	CDMA is cool, it follows the same principle as the two other except it uses a hashlike method 		to encode the content of it's Ethernet frame, thus all host can broadcast at the same time		given that the hash function is reliable (and that the receiver know the sender's code)
	

#Random Acess protocols:

Basicaly, the rule of thumb here is: every host broadcast at full bandwidth, and if there's a collision they wait a random-time-frame before retransmit.

Slotted ALOHA:

Here, all nodes are synchronised and broadcast only at the beginning of time frame (which is the time it takes for one node to transmit one frame)
If there's a collision, then all nodes will detect it before the end of the slot, 
and react accordingly...

Overall, the efficenciy of slotted ALOHA revolves around the long-run fraction of successful slot over all the time slots (the value is around 37% usually)


Carrier sense multiple access protocols:

CSMA is based around two principles: the nodes listen to each other and don't transmit if another one is transmitting.
If two node start broadcasting at the same time they both stop.

Now this is cool, but one problem is that the propagation delay of a signal A may take a non zero time to reach a node B and thus leave a small gap in time where B sense that no one is broadcasting and thus that he could broadcast...leading to collisiion !

This is why we have CSMA/CD (or CSMA with collision detection)

CD means that the the host A will transmit it's packet while at the same time monitoring for signal energy comming from other host via it's broadcast channel. It will stop the transmission if it detects energy, and wait for a specific time value T.


The duration of T is determined by the BEB algorithm, which goes like this:

for n collisions detected, each node choose a random value in the serie K
where K = {0,1,2,..,pow(2,n-1)};

thus when a node repeateadly sense a collision, it gets to choose randomly in an exponentialy larger set of Time value...


#taking turn protocols (Controlled)

these protocols often rely on a master or centralized guideline that fires transmission at every node level.

polling protocol: a master node polls each node 


Theses protocols include:
	ALOHA
	CSMA
	CSMA/CD
	CSMA/CA

