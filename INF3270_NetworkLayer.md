#Network layer


##keywords:
	Datagrams -> network layer packet
	Virtual-circuit -> a type of network architecture at the
	network layer

	switch vs router:
	packet switch -> transfers a input link packet to and output link packet
	router -> also does that
	
	*switch does the trasnfer based on link layer infos
	**router does it based on network layer info. (meaning one layer up)
	
##3 parts:
	-network-layer functions
		forwarding,routing,connection setup
	-forwarding
		-kinda like the routing inside the router
			with a forwarding table
		-It's exclusive to the inside of a router
	-routing
		-refers to the path a packet is taking 'outside' of
		 a router.
		-Made via an algorithm that can be centralized or decentralized
			centralized: an algorithm is executed on a central site
			and downloading routing information in all routers
			decentralized: each router runs a part of the algorithms
		-RIP/RIP2
		-Dijkstra


##network services:
	- the Internet network layer provides one standard for the service model
		- the best effort standard
			(meaning it tries to provide every service 
			 but doesn't garantee anything )

	- network layer services can still be connection/connectionless as in 
	  transport layer services
		differences: in the network layer these a host to host services
			(in transport layer it's process to process)

if a network architecture is connection oriented it's a virtual-circuit network
if it's connectionless it's a datagram network.

##virtual circuit network:
	a virtual circuit network implementation has network datagram possess a
	VC number that is associated with a router interface in a forwarding table
		-the forwarding table is of the form:
			inLink/VCin - outLink/VCout
		(so two pairs of VC/link number)
	The vc pair are established during the connection stage, 
	and removed when the connection closes...
	
	basicaly one can see it as decentralized, because each router is in charge 		of selecting an associated VCout for any new VCin
	(otherwise it wouldn't be feasible to maintain the circuit)
	
	A VC setup is completely a network layer service, and includes all routers 	along the path into the process, (contrary to the three way handshake which is 		a transport layer only service )


##Datagram Network:
	Connection-less, the datagram is stamped ith the adress of the destination
	and it leaves the "routing" to the routers along the way
	
	The router takes a look at the IP destination and forward to the assoc 
	Linkout

	The forwarding works according to the packet's prefix:


	ex:
		range		Linkout
		0000-0011	0
		0100-0101	1
		0110-1000	2
		1001-1111	3

	*this gives us the following prefix table:

		prefix		Linkout
		00		0
		010		1
		011		2	
		else		3
	**note that if theres is a common prefix the router selects 
		THE LONGEST PREFIX that matches.


#VC vs Datagram
	Note that Datagram Network forwarding tables are updated every 1-5 minutes 	
	by the routing algorithm

	While the VC network is updated everytime a new connection is setup

	Thus the VC is theoreticaly faster. 
	
	
#4.3 the inside of a Router..
												There are 3 main elements:
		-the ports (input/output)
			These ports are in charge of receiving link
			packets and performing the lookup in in the
			lookup table to know to which output port to
			send the packets

			each ports has a copy of the forwarding table

		-Switching fabrix:
			Hardware componet that performs the physical
			redirection inside the router
			ex
			    
			in1 -- out1	
			in2 /X out2
			
			the x zone is the switching fabric, in this
			case 1 and 2 are both redirected to out2

		-the Routing processor:
			processor in charge of the execution of the
			routing protocol and the maintneance of the lookup tables
		

	There are 3 main switching techniques
		memory switching (copy contents of input port into
			memory and recopy it to output buffer)

		bus switch (with a central bus)
			bus switching doesn't use a central routing
			processor
			
			the input packet receives a header
			representing the output port and is 
			then multicastest over the bus to all output, 
			only the corresponding one will keep it
		
			bus switching limits the router speed to the
			bus speed since only one packet can cross at a time.
		
		crossbar switching (with a multiplex/demux struct)
			
			Better than the bus because you can send
			multiple packets at the same time, as long as 
			they don't go to the same output port		
	
output port processing:

	-Queuing
	queuing is taken care of at the output port,so this is where
the packet dropping occurs
	-prioritising
In the case of packet dropping, the router has to prioritise who will
be dropped, different protocol existe for that problem specificaly	
	-optimal buffer size
Since queuing can occur and is potentially somehting you want to
avoid, the definition of an optimal buffer size is also relevent...


#4.4 The Internet Protocol
	
	IP packets are called datagrams: duh

		Header length is usualy 20 bytes
		Datagram Length is header + data

	A datagram contains the following fields:
	- IP version (ipv4 ou ipv6)
	- header length (usualy 20 bytes)
	- source/destination IP
	- checksum (for error detection)
	- protocol (6=tcp,17=udp )
	- TTL
	- flags
	- Options(note really seen i think ?)

*about fragmenting

the maximum transmission unit of a protocol is a variable
value,meaning a router may need to shrink a datagram by fragmenting it
into multiple pieces.

This is why fragmenting is used (note that it's not used in ipv6)

when fragmenting occurs in a datagram:

4 values becom important
Bytes: the size of payload
Id: a sort of packet id that serves to the receiver to batch all the
packets together
Offset: a value IN BYTES indicating how to insert the datat together
FLAGS: fragment == 1 if there's more 0 if it's the last fragment.



#IPV4 adressing

an ipv4 adress is 32 bit long
dot notation

to manage all the IP in the world we create subnets
a subnet mask is made to define the ADDRESS of the subnet

ex
	223.1.1.0/24 means the 24 leftmost bit define the subnet
	mask/adress

	thus any host in the subnet 223.1.1.0/24 should have the form
	223.1.1.xxx where xxx != 0

To attribute an IP adress on the server, a user can go trough th DHCP
protocol
	it's a plug and play protocol that automaticaly distributes
	and remove ip adresse for coming/going hosts.
	DHCP works in 4 steps:
		-discover (UDP BROADCAST DGCP discover message (posrt 67))
		-offer (DHCP server broadcasts a valid ip adress, with
		 a lease duration)
		-request (arriving host will send a request message to
		DHCP server for specific ip)
		-ACK the server will acknoledgw the new host choice

	* the duration of the lease can be in hours or even days
	* the reason why step 2 is a broadcast is because there can be
	  mutliple DHCP servers at the same time and they will all
	offer a possible ip adress (the host chooses at step 3)
	*There also is a possibilty that the user would want to renew
	the lease (this is a service that DHCP offers)..

#NAT

	

	

		
